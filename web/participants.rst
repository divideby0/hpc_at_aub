
Participants List
=====================

.. list-table::
  :class: borderless
  :widths: 1,5
  *  - .. image:: https://fbcdn-sphotos-a.akamaihd.net/hphotos-ak-ash2/168384_490546593397_502098397_5774267_4483201_n.jpg?sz=200
          :align: center
          :width: 100px
     - John Abou Jaoudeh, AUB
        Yeah Baby!
  *  - .. image:: http://lh5.googleusercontent.com/-AGd5TAirXGg/AAAAAAAAAAI/AAAAAAAABKY/UAw7VI_jh6E/photo.jpg?sz=200
          :align: center
          :width: 100px
     - Aron Ahmadia, Computational Scientist, KAUST
        Aron has been working as a Computational Scientist for King Abdullah University of Science and Technology since March of 2009, where he enjoys the occassional stint as lecturer (Parallel Computing Paradigms, Fall 2010), research time on Shaheen, their 65,536 core supercomputer, and the occasional dive with whale sharks.
  *  - .. image:: http://depts.washington.edu/struct/Faculty%20Research%20Interests/struct10.gif
          :align: center
          :width: 100px
     - George Turkiyyah, Professor, AUB
        The leading space is significant in RST, as well as the positions of the * and -  (It's like Fortran!).   Copy my example above to get the image to build right or just fill in your information and I'll take care of the silly RST formatting rules.
  *  - .. image:: http://2.bp.blogspot.com/_u1V4MoXxh9Q/SWCI29e4I6I/AAAAAAAADSE/IqEUMR7BJ-Y/s400/pug.jpg
          :align: center
          :width: 100px
     - Omar Rifai, Graduate Student, AUB
  *  - .. image:: http://2.bp.blogspot.com/_u1V4MoXxh9Q/SWCI29e4I6I/AAAAAAAADSE/IqEUMR7BJ-Y/s400/pug.jpg
          :align: center
          :width: 100px
     - Rany Kahil, GA, AUB
